export LN = ln -sf
export PWD = $(shell pwd)

TARGETS ?= fish vim tmux zsh

all: $(TARGETS)

$(TARGETS):
	$(MAKE) -C $@ install

update:
	@git submodule foreach git pull

.PHONY: $(TARGETS) all
