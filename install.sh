#!/bin/sh

# Set up the symlink to .zshrc

if [ ! -f ~/.zshrc ]; then
    ln -s ~/dotfiles/oh-my-zsh/custom/zshrc.symlink ~/.zshrc
fi

if [ ! -d ~/.oh-my-zsh ]; then
	ln -s ~/dotfiles/oh-my-zsh ~/.oh-my-zsh
fi

if [ ! -f ~/.vimrc ]; then
    ln -s ~/dotfiles/dotvim/vimrc ~/.vimrc
fi

if [ ! -d ~/.vim ]; then
	ln -s ~/dotfiles/dotvim ~/.vim
fi

if [ ! -f ~/.tmux.conf ]; then
    ln -s ~/dotfiles/tmux.conf ~/.tmux.conf
fi