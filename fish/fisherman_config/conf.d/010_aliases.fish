
if set -q $IS_MAC
    
    function ls --description 'Colorize output, add file type indicator, and put sizes in human readable'
        command ls -aFGh $argv
    end

    function ll --description 'Colorize output, add file type indicator, and put sizes in human readable'
        command ls -aFGhl $argv
    end

else if set -q $IS_LINUX
end




function .. --description 'Move up one directory'
    cd ..
end

function ... --description 'Move up two directories'
    cd ../..
end

function .... --description 'Move up three directories'
    cd ../../..
end

function ..... --description 'Move up four directories'
    cd ../../../..
end

function ...... --description 'Move up five directories'
    cd ../../../../..
end



function ip --description 'Get WAN IP'
    dig +short myip.opendns.com @resolver1.opendns.com
end


