# Installation: #

* Clone project to ~/dotfiles folder
```bash	
git clone  --recursive https://bitbucket.org/schneiderje/dotfiles.git dotfiles
git clone  --recursive git@bitbucket.org:schneiderje/dotfiles.git dotfiles
```
* Create symlinks
```bash
run the install script ./dotfiles/install.sh
```